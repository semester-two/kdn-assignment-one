#include <stdio.h>
#include <stdlib.h>

struct Node {
    int number;
    struct Node *next;
};

// Function to create a new node with given data
struct Node* createNode(int num) {
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

// Function to add a node at the end of the list
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

// Function to add a node at the beginning of the list
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to print all nodes in the list
void printList(struct Node *head) {
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;
        if (current != NULL) {
            printf(", ");
        }
    }
    printf("]\n");
}

// Function to delete a node by its key
void deleteByKey(struct Node **head, int key) {
    struct Node *temp = *head, *prev;

    if (temp != NULL && temp->number == key) {
        *head = temp->next;
        free(temp);
        return;
    }

    while (temp != NULL && temp->number != key) {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL) return;

    prev->next = temp->next;
    free(temp);
}

// Function to delete a node by its value
void deleteByValue(struct Node **head, int value) {
    struct Node *temp = *head, *prev = NULL;

    while (temp != NULL && temp->number == value) {
        *head = temp->next;
        free(temp);
        temp = *head;
    }

    while (temp != NULL) {
        while (temp != NULL && temp->number != value) {
            prev = temp;
            temp = temp->next;
        }

        if (temp == NULL) return;

        prev->next = temp->next;
        free(temp);
        temp = prev->next;
    }
}

// Function to insert a node after a given key
void insertAfterKey(struct Node **head, int key, int value) {
    struct Node *temp = *head;
    while (temp != NULL && temp->number != key) {
        temp = temp->next;
    }
    if (temp == NULL) {
        printf("Key not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(value);
    newNode->next = temp->next;
    temp->next = newNode;
}

// Function to insert a node after a given valu     e
void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    struct Node *temp = *head;
    while (temp != NULL && temp->number != searchValue) {
        temp = temp->next;
    }
    if (temp == NULL) {
        printf("Value not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(newValue);
    newNode->next = temp->next;
    temp->next = newNode;
}

int main() {
    struct Node *head = NULL;
    int choice, data;

    while (1) {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter key to delete: ");
                scanf("%d", &data);
                deleteByKey(&head, data);
                break;
            case 5:
                printf("Enter value to delete: ");
                scanf("%d", &data);
                deleteByValue(&head, data);
                break;
            case 6:
                printf("Enter key after which to insert: ");
                scanf("%d", &choice);
                printf("Enter data to insert: ");
                scanf("%d", &data);
                insertAfterKey(&head, choice, data);
                break;
            case 7:
                printf("Enter value after which to insert: ");
                scanf("%d", &choice);
                printf("Enter data to insert: ");
                scanf("%d", &data);
                insertAfterValue(&head, choice, data);
                break;
            case 8:
                printf("Exiting program.\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}


















































































    


















