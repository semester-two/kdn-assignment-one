#include <stdio.h>      
#include <math.h>

float calculateDiscriminant(float a, float b, float c) {
    return b*b - 4*a*c;
}

void findRoots(float a, float b, float c) {
    float discriminant, root1, root2;

    //Calculate the discriminant
    discriminant = calculateDiscriminant(a, b, c);
    
    //Check the nature of the roots
    if(discriminant > 0) {
        //Real and distinct roots
        root1 = (-b + sqrt(discriminant)) / (2*a);
        root2 = (-b - sqrt(discriminant)) / (2*a);
        printf("Roots are real and distinct: %.2f and %.2f\n", root1, root2);
    }
    else if (discriminant == 0) {
        //Real and equal roots
        root1 = root2 = -b /(2*a);
        printf("Roots are real and equal: %.2f\n", root1);
    }
    else {
        //Complex roots
        float realPart = -b / (2*a);
        float imaginaryPart = sqrt(-discriminant) / (2*a);
        printf("Roots are complex: %.2f + %.2fi and %.2f - %.2fi\n", realPart, imaginaryPart, realPart, imaginaryPart);
    }
}
int main() {
    float a, b, c;
    
    //Prompt user to enter coefficients
    printf("Enter coefficients (a, b, c): ");
    scanf("%f %f %f", &a, &b, &c);
   
    //Validate input: Ensure 'a' is not zero       
    if (a == 0) {
        printf("coefficient 'a' cannot be zero. Please enter valid coefficients.\n");
        return 1; // Exit with error code 1
    }
    // Call the function to find roots
    findRoots(a, b, c);
    return 0;
}



























